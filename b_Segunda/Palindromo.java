package b_Segunda;

public class Palindromo {
	
	public void Palindroma(String palavra) {
		
		//palavra=palavra.toUpperCase();  // MAIUSCUL�A
		palavra=palavra.toLowerCase(); // minuscula
		
		boolean palindromo = true;
		
		int i=0;
		int j= palavra.length()-1;
		
		
		while(i<j) {
			if(palavra.charAt(i) != palavra.charAt(j))
				palindromo = false;
			i++;
			j--;
		}
		
		System.out.print("A palavra: "+palavra);
		if(palindromo)
			System.out.println(" � um palindromo!");
		else
			System.out.println(" n�o � um palindromo!");
	}

}
