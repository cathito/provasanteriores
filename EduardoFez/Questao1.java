package EduardoFez;

public class Questao1 {
	/***
     * abrInicial: Caracter inicial de abertura de tag
     * abrFinal: Caracter final de abertura de tag
     * fechInicial: Caracter inicial de fechameto de tag
     * fechFinal: Caracter final de fechamento de tag
     */
    private char[] html;
    private int qtdChar, qtdAtributos, status, i = 0;
    private char ant = ' ';
    private int abrInicial, abrFinal, fechInicial, fechFinal = 0;
    private boolean DOC_HTML = true;

    /**
     * Valida sintáxe da string passada para saber se está no padrão HTML
     * @param str
     * @return boolean
     */
    public boolean validarHTML(String str)
    {
        html = str.toCharArray();
        for (this.i = 0; i < html.length; i++){
            qtdChar = i;
            validar(html[i]);
            if (!DOC_HTML){ break; }
        }
        int pos = --qtdChar;

        if (DOC_HTML){
            if (!openTagExist() && !closeTagExist() && qtdOpens() == 0){
                System.out.println("String de HTML correta!");
                System.out.println(qtdAtributos + " atributos de tags encontrados");
                return true;
            }else{
                System.out.println("Sintaxe error próximo ao caracter " + pos + ": " + html[pos] + " 'Algumas tags não foram fechadas'");
                System.out.println(qtdAtributos + " atributos de tags encontrados");
            }
        }else{
            System.out.println("Sintaxe error próximo ao caracter " + pos + ": " + html[pos]);
            System.out.println(qtdAtributos + " atributos de tags encontrados");
        }
        return false;
    }

    /**
     * Realiza as combinações de validação de caracter, abertura e fechamento de tag
     * @param c
     */
    private void validar(char c)
    {
        if (isHTML(c))
        {
            if (isAbertura(c))
            {
                if (status == 0)
                {
                    if (openTagExist()){ DOC_HTML = false; }else{ abrInicial++; }
                    ant = c;
                }else if (status == 1){
                    if (closeTagExist()){ DOC_HTML = false; }else{ fechInicial++; }
                }
            }
            else if (qtdOpens() > 0)
            {
                if (isFechamento(c))
                {
                    if (status == 0)
                    {
                        if (!openTagExist()){ DOC_HTML = false; }else{ abrFinal++; }
                    }else if (status == 1){
                        if (!closeTagExist()){ DOC_HTML = false; }else{ fechFinal++; }
                    }
                    ant = c;
                }else { DOC_HTML = false; }
            }else { DOC_HTML = false; }
        }
    }

    /**
     * Verifica a quantidade de tags abertas
     * Retornos:
     * = 0 não têm tags abertas
     * > 0 tem tags abertas
     * < 0 mais tag de fechamento que de abertura
     * @return int
     */
    private int qtdOpens(){ return (abrInicial + abrFinal) - (fechInicial + fechFinal); }

    /**
     * Verifica se existe alguma tag de abertura incompleta
     * @return boolean
     */
    private boolean openTagExist(){
        return ((abrInicial - abrFinal) == 0 ? false : true);
    }

    /**
     * Verifica se existe alguma tag de fechamento incompleta
     * @return boolean
     */
    private boolean closeTagExist(){
        return ((fechInicial - fechFinal) == 0 ? false : true);
    }

    /***
     * Verifica se é uma abertura de tag válida
     * @param c
     * @return
     */
   private boolean isAbertura(char c){
       int aux = ++qtdChar;
       if (c == '<' && ((aux > html.length-1) ? false : (html.length > 1) ? html[aux] == '/' : false)){
           ant = html[aux];
           i++;
           status = 1;
           return true;
       }else if (c == '<'){
           verificaAtributo();
           status = 0;
           return true;
       }
       return false;
   }

    /***
     * Verifica se é um fechamento de tag válida
     * @param c
     * @return boolean
     */
    private boolean isFechamento(char c){
        if (c == '>' && ant == '<'){
            status = 0;
            return true;
        }else if (c == '>' && ant == '/'){
            status = 1;
            return true;
        }
        return false;
    }

    /***
     * Verifica se é um caracter HTML válido
     * @param c
     * @return boolean
     */
    private boolean isHTML(char c) { return (c == '<' || c == '/' || c == '>' ) ? true : false; }

    /**
     * Verifica se é um caracter de atributo de tag válido
     * @param c
     * @return boolean
     */
    private boolean isAtributo(char c) { return (c == '"' || c == '=' ) ? true : false; }

    /**
     * Verifica se exite atributo na tag de abertura e se está correto
     * esse método não implementei muito bem por que já estou cansado querendo terminar logo
     */
    private void verificaAtributo()
    {
        int teste = i;
        int achouIgual = 0;
        int achouAspas = 0;
        int achouAspas2 = 0;
        char valor = ' ';

        while (teste < html.length){
            char letra = html[teste];

            if (letra == '>'){
                if ((achouAspas+achouAspas2+achouIgual) > 0 && (achouAspas+achouAspas2+achouIgual) < 3){
                    DOC_HTML = false;
                }else {
                    break;
                }
            }else if (isAtributo(letra)){
                if (achouIgual == 0 && achouAspas == 0 && achouAspas2 == 0){
                    if (letra == '='){ achouIgual = 1; valor = letra; }
                }
                else if (achouIgual == 1 && achouAspas == 0 && achouAspas2 == 0){
                    if (letra == '"' && valor == '='){ achouAspas = 1; valor = letra; } else { DOC_HTML = false; }
                }
                else if (achouIgual == 1 && achouAspas == 1 && achouAspas2 == 0){
                    if (letra == '"'){
                        valor = letra;
                        achouIgual = 0;
                        achouAspas = 0;
                        achouAspas2 = 0;
                        qtdAtributos++;
                    }
                }
            }
            teste++;
        }
    }
}
