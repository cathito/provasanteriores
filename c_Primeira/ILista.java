package c_Primeira;

public interface ILista{
	void adicionar(Object o,int pos) throws ListaException;
	void remover(int pos) throws ListaException;
	No obter(int pos) throws ListaException;
	int quantidade() throws ListaException;
	
}
