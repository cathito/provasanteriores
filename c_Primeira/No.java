package c_Primeira;

public interface No {
	No getProx();
	void setProx(No no);
	Object getInfo();
	void setInfo(Object o);

}
