package a_Primeira;


public class Pilha {
	
	private No topo;
	int qtdListas=0;
//----------------------------------------------------//
	
	public void empilhar(int valor) {
		No novo = new No(valor);

		if (topo == null){
			topo=novo;
		}else {
			novo.proximo=topo;
			topo=novo;
		}
		qtdListas++;
	}
//----------------------------------------------------//
	public void desempilhar() {
		if(topo!=null){
			topo=topo.proximo;
			qtdListas--;
		}else {
			System.out.println("A Pilha esta Vazia!");
		}

	}
//====================================================================//
	public boolean igual(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		
		Pilha other = (Pilha) obj;
		
		Pilha origin= new Pilha(); // Pilha p/ salvar Pilha
		Pilha parametro= new Pilha();
		
		boolean contem=true;
//========================================================//
		
		if (qtdListas != other.qtdListas)
			return false;
		else {
		//------------- Salvando -----------------//
			for (int i = 0; i <qtdListas; i++) {
				origin.empilhar(topo.dado);
				parametro.empilhar(other.topo.dado);
				
				desempilhar();
				other.desempilhar();
			}
		//----------------------------------------//
			
			while(origin.topo!=null){
				if(origin.topo.dado!=parametro.topo.dado) {
					// System.out.println("False");
					contem=false;
				}
				empilhar(origin.topo.dado);
				other.empilhar(parametro.topo.dado);
				origin.desempilhar();
				parametro.desempilhar();			
			}
		}
		
		return contem;
	}
//====================================================================//
	public void Listar() {
		No novo= topo;
		while(novo!=null) {
			System.out.println("| "+novo.dado+" |");
			novo=novo.proximo;
		}
		System.out.println("Tamanho da Pilha � : "+qtdListas);
	}
//----------------------------------------------------//
	
	
	

}
