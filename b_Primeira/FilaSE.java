package b_Primeira;

public class FilaSE {
	
	private NoFila inicio;
	private NoFila fim;
	int qtdListas=0;
//----------------------------------------------------//

	public void Adicionar(int valor) {
		NoFila novo = new NoFila(valor);

		if (inicio == null){
			this.inicio = novo;
			this.fim=inicio;
		}else {
			fim.proximo=novo;
			fim=novo;
		}
		qtdListas++;
		
	}
//----------------------------------------------------//
	public void Remover() {
		NoFila Atual= inicio;
		if(Atual!=null){
			inicio=Atual.proximo;
			qtdListas--;
		}

	}
//----------------------------------------------------//
	public void Listar() {
		NoFila novo= inicio;
		while(novo!=null) {
			System.out.println("| "+novo.dado+" |");
			novo=novo.proximo;
		}
	}
//----------------------------------------------------//
	
	public boolean equals(Object obj) {
		NoFila atualOriginal=inicio;
		
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		
		
		FilaSE other = (FilaSE) obj;
		NoFila atualParametro=other.inicio;
		
		
		if (qtdListas != other.qtdListas)
			return false;
		while(atualOriginal!=null) {
			if(atualOriginal.dado!=atualParametro.dado)
				return false;
			atualOriginal=atualOriginal.proximo;
			atualParametro=atualParametro.proximo;
		}
		return true;
	}
	
	
	
	
	
	
	
	

}
